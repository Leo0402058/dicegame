/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cejv569.peng.dicegameapp.presentation;

import com.cejve569.peng.dicegameapp.bean.DiceGameBean;
import javafx.application.Platform;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.Label;
import javafx.scene.control.*;
import javafx.scene.layout.GridPane;
import javafx.stage.Stage;

/**
 *
 * @author User
 */
public class WelcomePane{
    private static TextField userText ;
    private static TextField emailText;
    private static TextField amount;
    
    public static void regPane(){
        
        GridPane pane = new GridPane();
        //welcome user to play game
        Label welcome = new Label("Welcome to Dice Game!");
        pane.add(welcome, 0, 0, 2, 1);
        // create username textfield
        Label userNamelbl = new Label("Username:");
        pane.add(userNamelbl, 0, 1);
        
        userText = new TextField();
        userText.setMinWidth(100);
        userText.setMaxWidth(200);
        pane.add(userText, 1, 1);
        
        //create password textfield
        Label passwordlbl = new Label("Password: ");
        pane.add(passwordlbl, 0, 2);
        
        PasswordField password = new PasswordField();
        password.setMinWidth(100);
        password.setMaxWidth(200);
        pane.add(password, 1, 2);
        
        //create user email textfield
        Label emaillbl = new Label("Email address: ");
        pane.add(emaillbl, 0, 3);
        
        emailText = new TextField();
        emailText.setMinWidth(100);
        emailText.setMaxWidth(200);
        pane.add(emailText, 1, 3);
        
        //create amount of bet textfield
        Label amountBet= new Label("Amount Of Bet");
        pane.add(amountBet, 0, 4);
        
        amount = new TextField();
        amount.setMinWidth(100);
        amount.setMaxWidth(200);
        pane.add(amount, 1, 4);
        
        //create register button
        Button register = new Button("Register");
        register.setAlignment(Pos.CENTER_LEFT);
        register.setOnAction(e->registerClick());
        pane.add(register, 0, 5);
        
        //create exit button
        Button exit = new Button("Exit");
        exit.setAlignment(Pos.CENTER_RIGHT);
        exit.setOnAction(e->exitClick());
        pane.add(exit, 1, 5);
        
        Stage stage = new Stage();
        stage.setTitle("Dice Game Register");
        stage.setMinWidth(300);
        
        Scene scene = new Scene(pane);
        stage.setScene(scene);
        stage.showAndWait();
    }
    
    public static DiceGameBean registerClick(){
       DiceGameBean registerInfo = new DiceGameBean();
       registerInfo.setPlayerName(userText.getText());
       registerInfo.setPlayerEmail(emailText.getText());
       registerInfo.setPlayAmount(Double.parseDouble(amount.getText()));
       MessageBox.show("You have sucessfully regitered Dice Game", "DiceGame Information");
       return registerInfo;
    }
    
    public static void exitClick(){
       Platform.exit();
    
    }
}
