/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cejv569.peng.dicegameapp.presentation;

import javafx.geometry.Pos;
import javafx.scene.*;
import javafx.scene.control.*;
import javafx.scene.layout.VBox;
import javafx.stage.Modality;
import javafx.stage.Stage;

/**
 *
 * @author User
 */
public class MessageBox {
    public static void show(String message,String title){
       Stage stage = new Stage();
       stage.initModality(Modality.APPLICATION_MODAL);
       stage.setTitle(title);
       stage.setMinWidth(200);
       
       Label lbl = new Label();
       lbl.setText(message);
       
       Button btnOk = new Button();
       btnOk.setText("OK");
       btnOk.setOnAction(e->stage.close());
       
       VBox vbpane = new VBox();
       vbpane.getChildren().addAll(lbl,btnOk);
       vbpane.setAlignment(Pos.CENTER);
       
       Scene scene = new Scene(vbpane);
       stage.setScene(scene);
       stage.showAndWait();
       
       
    
   }
}
