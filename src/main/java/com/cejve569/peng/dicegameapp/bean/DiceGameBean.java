/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cejve569.peng.dicegameapp.bean;

import javafx.beans.property.DoubleProperty;
import javafx.beans.property.StringProperty;

/**
 *
 * @author User
 */
public class DiceGameBean {
     private StringProperty playerName;
     private StringProperty playerEmail;
     private DoubleProperty playAmount;

    public String getPlayerName() {
        return playerName.get();
    }

    public void setPlayerName(String playerName) {
        this.playerName.set(playerName);
    }
    
    public final StringProperty playerNameProperty(){
    
        return playerName;
    }
    
    public String getPlayerEmail() {
        return playerEmail.get();
    }

    public void setPlayerEmail(String playerEmail) {
        this.playerEmail.set(playerEmail);
    }
    public final StringProperty playerEmailProperty(){
    
        return playerEmail;
    }
    public Double getPlayAmount() {
        return playAmount.get();
    }
    

    public void setPlayAmount(Double playAmount) {
        this.playAmount.set(playAmount);
    }
     
    public final DoubleProperty playAmountProperty(){
    
        return playAmount;
    }

    @Override
    public String toString() {
        return "DiceGameBean{" + "playerName=" + playerName + ", playerEmail=" + playerEmail + ", playAmount=" + playAmount + '}';
    }
     
     
     
     
}
