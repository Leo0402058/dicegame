/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cejv569.peng.dicegameapp.presentation;

import com.cejve569.peng.dicegameapp.bean.DiceGameBean;
import javafx.application.Platform;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.*;

import javafx.scene.layout.*;
import javafx.stage.Stage;

/**
 *
 * @author User
 */
public class DiceGameGUI {
    
    Label modelbl;
    RadioButton passLine;
    RadioButton fieldBet;
    RadioButton any7;
    Label amountBet;
    static TextField betInput;
    Button startGame;
    Button exitGame;
    DiceGameBean dicegamebean;
   
//    public DiceGameGUI(DiceGameBean dicegamebean){
//       this.dicegamebean = dicegamebean;
//    }
  
    
    
    public void start(Stage primaryStage,DiceGameBean dicegamebean){
        this.dicegamebean = dicegamebean;
    
//    }
//    
//    private GridPane diceUserInterface(){
        GridPane grid = new GridPane();
        grid.setAlignment(Pos.CENTER);
        grid.setHgap(10);
        grid.setVgap(10);
        grid.setPadding(new Insets(25, 25, 25, 25));
        
        // Create game mode choice area
        Label modelbl = new Label("Please choose your game mode: ");
        grid.add(modelbl, 0, 0);
        //Set 3 game mode by using radio buttons and bind them together
        ToggleGroup gameMode = new ToggleGroup();
        
        passLine = new RadioButton("Pass Line");
        passLine.isSelected();
        passLine.setToggleGroup(gameMode);
        fieldBet = new RadioButton("Field Bet");
        fieldBet.setToggleGroup(gameMode);
        any7 = new RadioButton("Any 7");
        any7.setToggleGroup(gameMode);
        // Put all radio buttons into one node
        VBox modePane = new VBox();
        modePane.getChildren().addAll(passLine,fieldBet,any7);
        grid.add(modePane, 0, 1);
        
        // Create textfield for adding fund
        amountBet = new Label("Amount Of Bet");
        betInput = new TextField();
        
        
        HBox amountLine = new HBox();        
        amountLine.getChildren().addAll(amountBet,betInput);
        grid.add(amountLine, 0, 2);
        
        //Create start game button for registering the game
        startGame = new Button("Start");
        startGame.setOnAction(e ->startClick());
        grid.add(startGame, 0, 3);
        
        //Create exit button for closing the game
        exitGame = new Button("Exit");
        exitGame.setOnAction(e -> exitClick());
        grid.add(exitGame, 0, 4);
        
        primaryStage.setTitle("Dice Game Version 1.0");
       // GridPane root = diceUserInterface();
        Scene scene = new Scene(grid, 500, 600);
        primaryStage.setScene(scene);
        primaryStage.show();
        
       
    }
    
    public static void exitClick(){
    
        Platform.exit();
    
    }
    
    public static void startClick(){
        WelcomePane.regPane();
        
    }
    
    
}
